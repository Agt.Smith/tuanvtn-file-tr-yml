provider "aws" {
  region  = "us-east-1"
}

module "eks" {
  source = "terraform-aws-modules/eks/aws"

  cluster_name                    = "eks-cluster-mini-project"
  cluster_version                 = "1.21"
  cluster_endpoint_private_access = true
  cluster_endpoint_public_access  = true

#   cluster_encryption_config = [{
#     provider_key_arn = "ac01234b-00d9-40f6-ac95-e42345f78b00"
#     resources        = ["secrets"]
#   }]

  vpc_id     = "vpc-035d830f11c23bf37"
  subnet_ids = ["subnet-03aefd0942dd914c3", "subnet-07835e728373c8614"]

  # Self Managed Node Group(s)
#   self_managed_node_group_defaults = {
#     instance_type                          = "t2.micro"
#     update_launch_template_default_version = true
#     iam_role_additional_policies           = ["arn:aws:iam::aws:policy/AmazonSSMManagedInstanceCore"]
#   }

#   self_managed_node_groups = {
#     one = {
#       name = "spot-1"

#       public_ip    = true
#       max_size     = 3
#       min_size     = 1
#       desired_size = 2

#       use_mixed_instances_policy = true
#       mixed_instances_policy = {
#         instances_distribution = {
#           on_demand_base_capacity                  = 0
#           on_demand_percentage_above_base_capacity = 10
#           spot_allocation_strategy                 = "capacity-optimized"
#         }

#         override = [
#           {
#             instance_type     = "t2.micro"
#             weighted_capacity = "1"
#           }
#         ]
#       }

#       pre_bootstrap_user_data = <<-EOT
#       echo "foo"
#       export FOO=bar
#       EOT

#       bootstrap_extra_args = "--kubelet-extra-args '--node-labels=node.kubernetes.io/lifecycle=spot'"

#       post_bootstrap_user_data = <<-EOT
#       cd /tmp
#       sudo yum install -y https://s3.amazonaws.com/ec2-downloads-windows/SSMAgent/latest/linux_amd64/amazon-ssm-agent.rpm
#       sudo systemctl enable amazon-ssm-agent
#       sudo systemctl start amazon-ssm-agent
#       EOT
#     }
#   }

  # EKS Managed Node Group(s)

  eks_managed_node_group_defaults = {
    ami_type               = "AL2_x86_64"
    disk_size              = 8
    instance_types         = ["t2.medium"]
    vpc_security_group_ids = ["sg-07e5e3539b6728195"]
  }

  eks_managed_node_groups = {
    eks-node-jenkins = {
      min_size     = 1
      max_size     = 1
      desired_size = 1

      instance_types = ["t2.medium"]
      capacity_type  = "ON_DEMAND"
      labels = {
        GithubRepo  = "terraform-aws-eks"
        GithubOrg   = "terraform-aws-modules"
      }
      taints = {
        nginx = {
          key    = "jenkins"
          value  = "jenkinsGroup"
          effect = "NO_SCHEDULE"
        }
      }
      tags = {
        ExtraTag = "jenkins-server-tag"
      }
    }

    eks-node-web-database = {
      min_size     = 1
      max_size     = 3
      desired_size = 3

      instance_types = ["t2.medium"]
      //capacity_type  = "SPOT"
      capacity_type  = "ON_DEMAND"
      labels = {
        //Environment = "test"
        GithubRepo  = "terraform-aws-eks"
        GithubOrg   = "terraform-aws-modules"
      }
      taints = {
        web-database = {
          key    = "web-database"
          value  = "web-database-Group"
          effect = "NO_SCHEDULE"
        }
      }
      tags = {
        ExtraTag = "web-database-server-tag"
      }
    }
  }

#   # Fargate Profile(s)
#   fargate_profiles = {
#     default = {
#       name = "default"
#       selectors = [
#         {
#           namespace = "kube-system"
#           labels = {
#             k8s-app = "kube-dns"
#           }
#         },
#         {
#           namespace = "default"
#         }
#       ]

#       tags = {
#         Owner = "test"
#       }

#       timeouts = {
#         create = "20m"
#         delete = "20m"
#       }
#     }
#   }

#   tags = {
#     Environment = "dev"
#     Terraform   = "true"
#   }
}